import json
import csv
import random


def save_as_csv(filename, data):
  with open(filename, 'a') as csv_file:
    writer = csv.writer(csv_file)
    writer.writerows(data)

  csv_file.close()


def convert_to_csv():
  csv_data = [['Timestamp', 'Beacon1', 'Beacon2', 'Beacon3', 'Beacon4', 'Zone']]
  with open('firestore-export.json') as jf:
    data = json.load(jf)
    data_records = {}
    for zone_index in range(4):
      for key, value in data['recorded_data']['data'].items():
        if 'yd' in key and key.split('_')[3] == 'zone' + str(zone_index + 1):
          data_records[key.split('_')[1]] = value['data']

      dr = sorted(data_records.items())
      for dp0, dp1, dp2, dp3 in zip(dr[0][1], dr[1][1], dr[2][1], dr[3][1]):
        csv_data.append([dp0['timestamp'], dp0['rssi'], dp1['rssi'], dp2['rssi'], dp3['rssi'], zone_index + 1])
      
      data_records = {}

  jf.close()
  save_as_csv('yd_data.csv', csv_data)


convert_to_csv()
