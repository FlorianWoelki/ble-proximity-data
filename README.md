## What is this repository

With this repository you can get the data from firestore
and export it as a json file and therefore as .csv file.


## Setup the repository

You do the basic repository things, such as `npm install` to install all packages for this repository.

Then, you need to generate a service account key.

It looks like this:

```json
{
  "type": "service_account",
  "project_id": "ble-proximity",
  "private_key_id": "private_key_id",
  "private_key": "private_key",
  "client_email": "email",
  "client_id": "...",
  "auth_uri": "https://accounts.google.com/o/oauth2/auth",
  "token_uri": "https://oauth2.googleapis.com/token",
  "auth_provider_x509_cert_url": "https://www.googleapis.com/oauth2/v1/certs",
  "client_x509_cert_url": "https://www.googleapis.com/robot/v1/metadata/x509/firebase-adminsdk-78g13%40ble-proximity.iam.gserviceaccount.com"
}
```

You can find this file on Firebase, by doing the following steps:

1. Go to Firebase and select your project (ble-proximity)
2. Click on the left hand site on settings (next to Project Overview) and then on "Project Settings"
3. Then you click on the menu item "Office Accounts" ("Dienstkonten"), and then on "Generate new private key"
4. After that you drag and drop your .json file in this project and rename this file to "serviceAccountKey.json"

Then you are good to go.


## How to get the data

* Node and Python need to be installed!

1. You need to type into your console `node export.js`. This will download all the data from firestore and it will create a new file namely "firestore-export.json".
2. After that you need to modify the Python script and then you can easily run `python create_csv.py`to get all the selected data in a .csv format

